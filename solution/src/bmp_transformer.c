//
// Created by ticug on 31.10.2022.
//

#include "bmp_transformer.h"
#include <malloc.h>
#include <memory.h>
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

void add_to_char(char* to, const char* from, size_t len) {
    for (size_t i = 0; i < len; ++i) {
        to[i] = from[i];
    }
}

struct bmp_header create_bmp_header(const struct image* img) {
    size_t header_length = 54;
    int padding = 4 - (int) ((img->width* 3) % 4);
    size_t width = img->width * sizeof(struct pixel) + padding;
    char type[] = {'B', 'M'};

    struct bmp_header header = {*(uint16_t*) type, header_length + width * img->height, 0, header_length, 40, img->width, img->height, 1, 24, 0, 0, 0, 0, 0, 0};

    return header;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    fseek(in, 0, SEEK_END);
    size_t size = ftell(in);

    char* array = calloc(size, 1);

    fseek(in, 0, 0);
    size_t read = fread(array, 1, size, in);

    struct bmp_header* header = (struct bmp_header*) array;

    if (read < 50) return READ_INVALID_HEADER;
    if (header->bfType != 0x4D42) return READ_INVALID_SIGNATURE;
    if (header->biBitCount != 24) return READ_INVALID_BITS;

    img->width = header->biWidth;
    img->height = header->biHeight;

    size_t header_length = header->bOffBits;
    int padding = 4 - (int) (header->biWidth * sizeof(struct pixel)) % 4;
    img->data = malloc(sizeof(struct pixel) * header->biWidth * header->biHeight);

    for (size_t i = 0; i < header->biHeight; ++i) {
        for (size_t j = 0; j < header->biWidth; ++j) {
            img->data[i * header->biWidth + j].b = array[header_length + i * (padding + sizeof(struct pixel) * header->biWidth) + j * sizeof(struct pixel)];
            img->data[i * header->biWidth + j].g= array[header_length + i * (padding + sizeof(struct pixel) * header->biWidth) + j * sizeof(struct pixel) + 1];
            img->data[i * header->biWidth + j].r = array[header_length + i * (padding + sizeof(struct pixel) * header->biWidth) + j * sizeof(struct pixel) + 2];
        }
    }

    free(array);

    return (enum read_status) {0};
}


enum write_status to_bmp( FILE* out, struct image const* img ) {
    size_t header_length = 54;
    int padding = 4 - (int) ((img->width* 3) % 4);
    size_t width = img->width * sizeof(struct pixel) + padding;
    char* array = malloc(header_length + width * img->height);

    struct bmp_header* header = malloc(sizeof(struct bmp_header));
    *header = create_bmp_header(img);

    add_to_char(array, (char*) header, sizeof(struct bmp_header));

    for (size_t i = 0; i < img->height; ++i) {
        for (size_t j = 0; j < img->width; ++j) {
            array[header_length + i * (padding + sizeof(struct pixel) * img->width) + j * sizeof(struct pixel)] = (char) img->data[i * img->width+ j].b;
            array[header_length + i * (padding + sizeof(struct pixel) * img->width) + j * sizeof(struct pixel) + 1] = (char) img->data[i * img->width+ j].g;
            array[header_length + i * (padding + sizeof(struct pixel) * img->width) + j * sizeof(struct pixel) + 2] = (char) img->data[i * img->width+ j].r;
        }
        for (int j = 0; j < padding; ++j) {
            array[header_length + i * padding + (i + 1) * sizeof(struct pixel) * img->width + j] = 0;
        }
    }

    fwrite(array, 1, header_length + width * img->height, out);

    free(header);
    free(array);

    return (enum write_status) {0};
}
