//
// Created by gabriela on 01.11.22.
//

#include "image_rotate.h"
#include <malloc.h>
#include <stddef.h>

struct image rotate( struct image const source ) {
    struct image result = {.width = source.height, .height = source.width};
    result.data = malloc(result.width * result.height * sizeof(struct pixel));

    if (source.height == 0 || source.width == 0) return result;

    for (size_t i = (size_t) source.width;  i > 0; --i) {
        for (size_t j = 0; j < source.height; ++j) {
            result.data[(i - 1) * result.width + (result.width- j - 1)] = source.data[i - 1 + j * source.width];
        }
    }

    return result;
}
