//
// Created by gabriela on 01.11.22.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_ROTATE_H
#define IMAGE_TRANSFORMER_IMAGE_ROTATE_H

#include "bmp_transformer.h"

struct image rotate( struct image const source );

#endif //IMAGE_TRANSFORMER_IMAGE_ROTATE_H
