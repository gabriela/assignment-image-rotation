#include "bmp_transformer.h"
#include "image_rotate.h"
#include <malloc.h>
#include <stdio.h>

struct image* read_file(const char* filename) {
    struct image* img = malloc(sizeof(struct image));
    FILE* file = fopen(filename, "rb");

    from_bmp(file, img);

    fclose(file);

    return img;
}

void write_file(const char* filename, struct image* image) {
    FILE* file = fopen(filename, "wb");

    enum write_status status = to_bmp(file, image);
    if(status) fprintf(stderr, "Error while writing in the file");

    fclose(file);
}

int main(int argc, char** argv) {
    (void) argc;
    (void) argv; // suppress 'unused parameters' warning

    struct image* img = read_file(argv[1]);
    struct image new_img = rotate(*img);
    write_file(argv[2], &new_img);

    free(img->data);
    free(img);
    free(new_img.data);

    return 0;
}
