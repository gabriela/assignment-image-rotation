#pragma once

#include <inttypes.h>
#include <malloc.h>
#include <stddef.h>

#include "dimensions.h"

#pragma pack(push, 1)
struct pixel {
  uint8_t components[3];
};
#pragma pack(pop)

struct image {
  struct dimensions size;
  struct pixel* data;
};

struct image image_create( struct dimensions size );
void image_destroy( struct image* image );
